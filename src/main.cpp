//APVFeedBakE CRT Firmware V0.0.0
//James Anderson and Shaurjya Banerjee 2018

#include <Arduino.h>

#include <TVout.h>
#include <TVoutfonts/fontALL.h>

#define SCREEN_X        120
#define SCREEN_Y        96

TVout TV;
extern TVout_vid display;

// I/O definitions
const byte button1_pin = 13;
const byte button2_pin = 12;
const byte button3_pin = 11;
const byte button4_pin = 10;

const byte pot1_pin = A0;
const byte pot2_pin = A1;
const byte pot3_pin = A2;
const byte pot4_pin = A3;

const byte trig1_pin = 5;
const byte trig2_pin = 6;


//UI state
byte button1_state = 0;
byte button2_state = 0;
byte button3_state = 0;
byte button4_state = 0;
byte trig1_state = 0;
byte trig2_state = 0;

int pot1_val = 0;
int pot2_val = 0;
int pot3_val = 0;
int pot4_val = 0;

int t = 69;
byte offs = 0;

const int button_delay = 300;

int screen_size = (SCREEN_X / 8) * SCREEN_Y * sizeof(unsigned char);

int hres = 0;
int vres = 0;
int n_pixels = 0;

void control_poller();

void setup()
{
    pinMode(button1_pin, INPUT);
    pinMode(button2_pin, INPUT);
    pinMode(button3_pin, INPUT);
    pinMode(button4_pin, INPUT);
    pinMode(trig1_pin, INPUT);
    pinMode(trig2_pin, INPUT);

    for (int i = 0; i < 4; i++) {
        pinMode(i, OUTPUT);
        digitalWrite(i, HIGH);
    }

    TV.begin(NTSC, 120, 96);
    hres = TV.hres();
    vres = TV.vres();
    n_pixels = hres * vres;
    TV.select_font(font8x8);

    DDRB |= (1 << 5);
}

void loop()
{
    t++;
    // int bbval = ((t << 1) ^ ((t << 1) + (t >> 7) & t >> 12)) | t >> (4 - (1 ^ 7 & (t >> 15))) | t >> 7;
    int bbval = ((t * (t >> 8 | t >> 9) & 46 & t >> 8)) ^ (t & t >> 13 | t >> 6);
    control_poller();

    int8_t shiftx = 1, shifty = 1;

    uint8_t *dst = display.screen;
    uint8_t *src = display.screen + display.hres;
    uint8_t *end = display.screen + display.vres*display.hres;
    uint8_t tmp = 0;
    // int mode = map(pot1_val, 1024, 0, 0, 7);
    int mode = (bbval >> 1) & 7;
#if 1
    switch (mode) {
        case 0: {
            TV.screen[bbval & 0xff] ^= bbval >> 12;
            break;
        }
        case 1: {
            TV.screen[bbval >> 3] |= (bbval & 0xff);
            break;
        }
        case 2: {
            TV.screen[bbval >> 3] ^= (bbval & 0xff);
            break;
        }
        case 3: {
            while (src <= end) {
                tmp = *dst;
                *dst = *src;
                *src = tmp;
                dst++;
                src++;
            }
            break;
        }
        case 4: {
            TV.screen[bbval >> 3] = ~(bbval & 0xff);
            break;
        }
        case 5: {
            TV.select_font((const unsigned char*) 0x100);
            TV.write((char*)bbval, (bbval >> 3) & 7);
            break;
        }
        case 6: {
            TV.select_font(font6x8);
            TV.write((char*)bbval, bbval & 0xc);
            break;
        }
        case 7: {
            while (src <= end) {
                tmp = *dst;
                *dst = ~(*src);
                *src ^= tmp;
                dst++;
                src++;
            }
            break;
        }
        default: {
            break;
        }
    }
#else
    char a = ' ';
    TV.write(&a, 1);
    // TV.write((char*)(bbval & 0xfc), 1);
    offs = bbval & 1;
    offs <<= 2; // the font pointer is used to read program memory, needs to be multiple of 4 (apparently)
    TV.select_font((const unsigned char*) 256 + offs);
#endif
    PORTD = bbval & 0b00001111;
    PORTB = PORTB ^ (1 << 5);
}

void control_poller()
{
    button1_state = digitalRead(button1_pin);
    button2_state = digitalRead(button2_pin);
    button3_state = digitalRead(button3_pin);
    button4_state = digitalRead(button4_pin);
    trig1_state = digitalRead(trig1_pin);
    trig2_state = digitalRead(trig2_pin);

    pot1_val = analogRead(pot1_pin);
    pot2_val = analogRead(pot2_pin);
    pot3_val = analogRead(pot3_pin);
    pot4_val = analogRead(pot4_pin);
}

